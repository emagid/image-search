<?php

class announcementsController extends adminController {
	
	function __construct(){
		parent::__construct("Announcement");
	}
	
	function index(Array $params = []){

		$this->_viewData->hasCreateBtn = true;		
		parent::index($params);
	}

	function update_post(){
	  
        parent::update_post();
    }
}