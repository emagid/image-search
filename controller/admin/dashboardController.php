<?php

use Emagid\Core\Membership,
Emagid\Html\Form,
Model\Admin,
Model\Banner; 

class dashboardController extends adminController {
	
	function __construct() {
        parent::__construct('Dashboard');
    }

	public function index(Array $params = []){
        $sql = "SELECT * FROM (".
                " (SELECT gif.id as gif_id, NULL AS pic_id, gif.image AS gif_url, NULL as pic_url, gif.insert_time FROM gif WHERE gif.active = 1)".
                " UNION ALL".
                " (SELECT NULL AS gif_id, Snapshot_Contact.id as pic_id, NULL as gif_url, '/content/uploads/Snapshots/' || Snapshot_Contact.image as pic_url, Snapshot_Contact.insert_time FROM snapshot_contact WHERE snapshot_contact.active = 1)".
                ") results".
                " ORDER BY insert_time DESC LIMIT 5";

        global $emagid;

        $db = $emagid->getDb();


        $latest = $db->execute($sql) ;
        $this->_viewData->latest = $latest;

		$this->_viewData->page_title = 'Dashboard';

        $this->_viewData->client_lists = \Model\Client::getList(['where'=>"active = 1"]);

        $this->_viewData->client = \Model\Client::getCount(['where'=>"active=1"]);
        $this->_viewData->project = \Model\Project::getCount(['where'=>"active=1"]);
        $this->_viewData->image = \Model\Project_Image::getCount(['where'=>"active=1"]);
        $this->_viewData->complete_image = \Model\Project_Image::getCount(['where'=>"active=1 and status = 1"]);

        $this->_viewData->announcements = \Model\Announcement::getList(['where'=>"active=1"]);
       
		$this->loadView($this->_viewData);
	}



    public function client(Array $params = []){
        $sql = "SELECT * FROM (".
                " (SELECT gif.id as gif_id, NULL AS pic_id, gif.image AS gif_url, NULL as pic_url, gif.insert_time FROM gif WHERE gif.active = 1)".
                " UNION ALL".
                " (SELECT NULL AS gif_id, Snapshot_Contact.id as pic_id, NULL as gif_url, '/content/uploads/Snapshots/' || Snapshot_Contact.image as pic_url, Snapshot_Contact.insert_time FROM snapshot_contact WHERE snapshot_contact.active = 1)".
                ") results".
                " ORDER BY insert_time DESC LIMIT 5";

        global $emagid;

        $db = $emagid->getDb();


        $latest = $db->execute($sql) ;
        $this->_viewData->latest = $latest;

        $this->_viewData->page_title = 'Dashboard';

        $this->_viewData->client_lists = \Model\Client::getList(['where'=>"active = 1"]);

        $this->_viewData->client = \Model\Client::getCount(['where'=>"active=1"]);
        $this->_viewData->project = \Model\Project::getCount(['where'=>"active=1"]);
        $this->_viewData->image = \Model\Project_Image::getCount(['where'=>"active=1"]);
        $this->_viewData->complete_image = \Model\Project_Image::getCount(['where'=>"active=1 and status = 1"]);

        $this->_viewData->announcements = \Model\Announcement::getList(['where'=>"active=1"]);
       
        $this->loadView($this->_viewData);
    }


}