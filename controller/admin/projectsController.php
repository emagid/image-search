<?php

class projectsController extends adminController {
	
	function __construct(){
		parent::__construct("Project", "projects");
	}
	
	function index(Array $params = []){
		// $this->_viewData->hasCreateBtn = true;
		parent::index($params);
	}

	function update(Array $arr = []){
		$c_name = urldecode($arr['id']);;
		$this->_viewData->client = \Model\Client::getItem(null,['where'=>"name = '$c_name'"]);
		// $this->loadView($this->_viewData);
		parent::update($arr);
	}

	function update_post(){
	  
        parent::update_post();
    }

    function update_image(Array $arr = []){
    	
    	$case_id = $arr['id'];
    	$this->_viewData->pcases = \Model\Project::getItem(null,['where'=>"id = $case_id"]);
    	parent::update($arr);
    }

    function update_image_post(){    
        if($_POST['image'] == ''){
            
        } else {
            $fileName = $_POST['image'];
            $tmpName = $_POST['image'];

            $case_image = new \Model\Project_Image();
            $case_image->image = $_POST['image'];
            // $case_image->status = $_POST['status'];
            $case_image->client_id = $_POST['client_id'];
            $case_image->project_id = $_POST['project_id'];
            $case_image->name = $_POST['name'];

            $fileName = uniqid()."_".basename($fileName);
            $fileName = str_replace(' ', '_', $fileName);
            $fileName = str_replace('-', '_', $fileName);

            move_uploaded_file(tmpName, UPLOAD_PATH . 'projects' . DS. $fileName );

            $case_image->save();
        }

        redirect(ADMIN_URL.'projects/project/'.$_POST['project_id']);
    }

    function project(Array $params = []){

    	
    	$case_id = $params['id'];
    	$this->_viewData->project = \Model\Project::getItem(null,['where'=>"id = $case_id"]);
    	
    	$this->_viewData->case_images = \Model\Project_Image::getList(['where'=>"project_id = $case_id"]);
    	
    	$this->loadView($this->_viewData);
    }

    // function upload_from_drive(){

    //     $redirect_uri = ADMIN_URL."/projects/update_image";
    //     $client = new Google_Client();
    //     $client->setAuthConfig(ROOT_DIR.'client_secret.json');
    //     $client->setredirectUri($redirect_uri);
    //     $client->addScope("https://www.googleapis.com/auth/drive");
    //     $service = new Google_Service_Drive($client);

    //     if(isset($_GET['code'])){
    //         $token = $client->fetchAccessTokenWithAuthCode($_GET['code']);
    //         $client->setAccessToken($token);
    //         // header('')
    //     }

    //     if(!empty($_SESSION['upload_token'])) {
    //         $client->setAccessToken($_SESSION['upload_token']);
    //         if($client->isAccessTokenExpired()) {
    //             unset($_SESSION['upload_token']);
    //         }
    //     }
    //     else{
    //         $authUrl = $client->createAuthUrl();        
    //     }

    //     if($client->getAccessToken()){

    //     }
    // }

    function upload_images($params)
    {
        header("Content-type:application/json");
        $data = [];
        $data['success'] = false;
        $data['redirect'] = false;
        $id = (isset($params['id']) && is_numeric($params['id']) && $params['id'] > 0) ? $params['id'] : 0;
        if ((int)$id > 0) {
            $project = \Model\Project::getItem($id);
            if ($project == null) {
                $data['redirect'] = true;
            }
            
            $this->save_upload_images($_FILES['file'], $project->id, $project->client_id, UPLOAD_PATH . 'projects' . DS, []);
            $data['success'] = true;
        }

        //print_r($_FILES);
        echo json_encode($data);
        exit();
    }

    public function save_upload_images($files, $obj_id, $cobj_id, $upload_path, $sizes = [])
    {
        
    	$counter = 1;
        // $map = new \Model\Product_Map();
        // $mapId = $map->getMap($obj_id, "product")->id;
        // get highest display order counter if any images exist for specified listing
        $image_high = \Model\Project_Image::getItem(null, ['where'=>'project_id='.$obj_id,'sort'=>'DESC']);
        if ($image_high != null) {
            $counter = $counter + 1;
        }
        foreach ($files['name'] as $key => $val) {
        	if($files['error'][$key] != 0){
        		continue;
        	}
            $project_image = new \Model\Project_Image();
            $temp_file_name = $files['tmp_name'][$key];
            $file_name = uniqid() . "_" . basename($files['name'][$key]);
            $file_name = str_replace(' ', '_', $file_name);
            $file_name = str_replace('-', '_', $file_name);
            		
            $allow_format = array('jpg', 'png', 'gif', 'JPG');
            $filetype = explode("/", $files['type'][$key]);
            $filetype = strtolower(array_pop($filetype));
            //$filetype = explode(".", $files['name'][$key]); //$file['type'] has value like "image/jpeg"
            //$filetype = array_pop($filetype);
            if ($filetype == 'jpeg' || $filetype == "JPG") {
                $filetype = 'jpg';
            }

            if (in_array($filetype, $allow_format)) {

                $img_path = compress_image($temp_file_name, $upload_path . $file_name, 60, $files['size'][$key]);
                if ($img_path === false) {
                	
                    move_uploaded_file($temp_file_name, $upload_path . $file_name);
                }

                if (count($sizes) > 0) {
                    foreach ($sizes as $key => $val) {
                        if (is_array($val) && count($val) == 2) {
                            resize_image_by_minlen($upload_path,
                                $file_name, min($val), $filetype);

                            $path = images_thumb($file_name, min($val),
                                $val[0], $val[1], file_format($file_name));
                            $image_size_str = implode('_', $val);
                            copy($path, $upload_path . $image_size_str . $file_name);
                        }
                    }
                }
                
                $project_image->image = $file_name;
                $project_image->project_id = $obj_id;
                $project_image->client_id = $cobj_id;
                $project_image->save();
            }
            $counter++;
        }
    }

    function image(Array $params = []){
        // $this->_viewData->hasCreateBtn = true;
        $imageId = $params['id'];
        $this->_viewData->currentImage = \Model\Project_Image::getItem(null,['where'=>"id = $imageId"]);
        parent::index($params);
    }

    function image_search(Array $arr = []){
        
        parent::index($arr);
    }

    function client_image(Array $params = []){
        // $this->_viewData->hasCreateBtn = true;
        parent::index($params);
    }
  
}