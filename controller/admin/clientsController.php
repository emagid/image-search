<?php

class clientsController extends adminController {
	
	function __construct(){
		parent::__construct("Client", "clients");
	}
	
	function index(Array $params = []){

		$this->_viewData->hasCreateBtn = true;		
		parent::index($params);
	}

	function settings(Array $params = []){
		parent::index($params);
	}

	function update_post(){
	  
        parent::update_post();
    }
  
  	function client(Array $params = []){

  		$this->_viewData->client_details = \Model\Client::getItem(null,['where'=>"id = ". $params['id']]);
  		$this->_viewData->cases = \Model\Project::getList(['where' => "client_id = ". $params['id']]);
  		$this->loadView($this->_viewData);
  	}
}