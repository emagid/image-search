<?php

use Twilio\Rest\Client;

class homeController extends siteController {
        function __construct(){
        parent::__construct();
    }    
    public function index(Array $params = []){


        $this->viewData->banners = \Model\Banner::getList(['where'=>"active='1'"]);
        $this->viewData->mainBanner = \Model\Banner::getList(['where'=>"active = 1 and featured_id = 0", 'orderBy'=>"banner_order asc"]);
        $this->viewData->jewelry = \Model\Jewelry::getList(['where' => "active = 1 and featured = 1 and discount != 0", 'orderBy' => "RANDOM()", 'limit' => 2]);

        $this->viewData->diamond = \Model\Product::getItem(null, ['where' => "active = 1 and featured = 1 and discount != 0 and quantity > 0", 'orderBy' => "RANDOM()", 'limit' => 1]);
        $this->viewData->ring = \Model\Ring::getItem(null, ['where' => "active = 1 and featured = 1 and discount != 0", 'orderBy' => "RANDOM()", 'limit' => 1]);
        
        $this->loadView($this->viewData);
    }

    function login(Array $params = []){
        $this->loadView($this->viewData);
    }

    function photobooth(Array $params = []){
        $this->loadView($this->viewData);
    }


    function products(Array $params = []){
        $this->loadView($this->viewData);
    }

    function security(Array $params = []){
        $this->loadView($this->viewData);
    }


    function trivia(Array $params = []){
        $this->loadView($this->viewData);
    }

    function healthcare(Array $params = []){
        $this->loadView($this->viewData);
    }

    function privatecloud(Array $params = []){
        $this->loadView($this->viewData);
    }

    function hybrid(Array $params = []){
        $this->loadView($this->viewData);
    }

    function management(Array $params = []){
        $this->loadView($this->viewData);
    }

    function disaster(Array $params = []){
        $this->loadView($this->viewData);
    }

    function ransomeware(Array $params = []){
        $this->loadView($this->viewData);
    }

    function ny1(Array $params = []){
        $this->loadView($this->viewData);
    }

    public function index_post()
    {

        $businesses = \Model\Business::getList(['where'=>"lower(name) like lower('%{$_POST['search']}%')"]);

        $this->viewData->businesses = $businesses;
        $this->loadView($this->viewData);
    }

    public function query(){
        $businesses = \Model\Business::getList(['where'=>"lower(name) like lower('%{$_POST['q']}%')"]);
        $res = [];
        foreach ($businesses as $business) {
            $res[] = ['label' => $business->name];
        }
        $this->toJson($res);
    }

    public function getEmployees_post(){
        if(isset($_SESSION['business_name'])){
            $business_name = $_SESSION['business_name'];
            // var_dump($business_name);
            $employees = \Model\Employee::getList(['where'=>"lower(business_name) = lower('$business_name')"]);
            foreach ($employees as $employee) {
                $_SESSION['employee_email'] = $employee->email;
                $_SESSION['employee_phone'] = $employee->phone;
            }
            $this->toJson($employees);
        }
    }

    public function sendEmailtoBusiness(){
        // var_dump($_POST);
        if(!empty($_POST['name']) && !empty($_POST['subject']) && !empty($_POST['email']) && !empty($_POST['message']) && !empty($_POST['bizemail']) && (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) === false)){


            $response = ['status'=>false,
                     'msg'=>'Some problem occurred, please try again.'];

            $name = $_POST['name'];
            $subject = $_POST['subject'];
            $email = $_POST['email'];
            $message = $_POST['message'];
            
            $to = $_POST['bizemail'];
            
            $email = new \Email\MailMaster();
            $mergeTags = [
                'CONTENT'=>$message.$name
            ];
            $this->toJson($email->setTo(['email' => $to, 'name' => ucwords($name), 'type' => 'to'])->setSubject($subject)->setTemplate('webairkiosk')->setMergeTags($mergeTags)->send());
            $response['status'] = true;
            $response['msg'] = 'Email Successfully sent';

        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }
    
    public function sendEmailtoEmployee(){
        // var_dump($_POST);
        if(isset($_SESSION['employee_email']) && !empty($_POST['name']) && !empty($_POST['subject']) && !empty($_POST['email']) && !empty($_POST['message']) && (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) === false)){


            $response = ['status'=>false,
                     'msg'=>'Some problem occurred, please try again.'];

            $name = $_POST['name'];
            $subject = $_POST['subject'];
            $email = $_POST['email'];
            $message = $_POST['message'];
            
            $to = $_SESSION['employee_email'];
            var_dump($to);
            
            $email = new \Email\MailMaster();
            $mergeTags = [
                'CONTENT'=>$message." from - ". $name
            ];
            $this->toJson($email->setTo(['email' => $to, 'name' => ucwords($name), 'type' => 'to'])->setSubject($subject)->setTemplate('webairkiosk')->setMergeTags($mergeTags)->send());
            $response['status'] = true;
            $response['msg'] = 'Email Successfully sent';

        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function sendTexttoEmployee(){
        // var_dump($_POST);
        $sid = ACCOUNT_SID;
        $token = AUTH_TOKEN;
        $client = new Client($sid, $token);
        $from = FROM_NUMBER;

        if(isset($_SESSION['employee_phone']) && !empty($_POST['name']) && !empty($_POST['message']) && !empty($_POST['phone']))
        {
            
            $response = ['status'=>false,
                     'msg'=>'Some problem occurred, please try again.'];

            $toNumber = $_SESSION['employee_phone'];
            var_dump($toNumber);
            $name = $_POST['name'];
            $message = $_POST['message'];

            $client->messages
            ->create(
                $toNumber,
                array(
                    "from" => $from,
                    "body" => "Following text Message from - ".$name. ". Message:- ".$message,
                )
            );
            

        }
        // header('Content-Type: application/json');
        // echo json_encode($response);
    }
}
