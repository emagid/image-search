
--March 19, 2018
ALTER TABLE public.administrator ADD COLUMN phone character varying;


CREATE SEQUENCE public.client_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE public.client
(
  id integer NOT NULL DEFAULT nextval('client_id_seq'::regclass),
  active smallint DEFAULT 1,
  insert_time timestamp without time zone DEFAULT now(),
  name character varying NOT NULL,
  email character varying NOT NULL,
  phone character varying NOT NULL,
  CONSTRAINT client_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);


CREATE SEQUENCE public.project_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE public.project
(
  id integer NOT NULL DEFAULT nextval('project_id_seq'::regclass),
  active smallint DEFAULT 1,
  insert_time timestamp without time zone DEFAULT now(),
  name character varying ,
  client_id integer references client(id),
  CONSTRAINT project_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

CREATE SEQUENCE public.project_image_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE public.project_image
(
  id integer NOT NULL DEFAULT nextval('project_image_id_seq'::regclass),
  active smallint DEFAULT 1,
  insert_time timestamp without time zone DEFAULT now(),
  image character varying,
  status smallint,
  project_id integer references project(id),
  client_id integer references client(id),
  CONSTRAINT project_image_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

--April 2. 2018

CREATE SEQUENCE public.announcement_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE public.announcement
(
  id integer NOT NULL DEFAULT nextval('announcement_id_seq'::regclass),
  active smallint DEFAULT 1,
  insert_time timestamp without time zone DEFAULT now(),
  title character varying,
  description character varying,
  CONSTRAINT announcement_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

--April 6,2018
ALTER TABLE project_image ADD COLUMN name character varying;