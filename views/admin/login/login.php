
<section class='login_page'>
	<div class='login_form'>
		<h1>LOGIN</h1>
		<p>Sign in to manage your accounts</p>
		<div class="form-box" id="login-box">
		
		<?php if(isset($model->errors)){ ?>
		<?php foreach ($model->errors as $error) { ?>
		<div class="alert alert-danger"><?php echo $error; ?></div>
		<?php } ?>
		<?php } ?>
		<?php if(isset($model->message)){ ?>
		<p><?php echo $model->message; ?></p>
		<?php } ?>
	<!--	<div class="header">ADMIN</div>-->
		<form id="admin_login" action="<?= ADMIN_URL ?>login" method="post">
			<div class="body bg-gray">
				<div class="form-group">
					<input type="text" name="username" class="form-control" placeholder="Username"/>
				</div>
				<div class="form-group">
					<input type="password" name="password" class="form-control" placeholder="Password"/>
				</div>
				<div class="form-group">
					<button type="submit" class="btn bg-black btn-block btn-lg button_border">Sign in</button>
				</div>

			</div>
		</form>
		</div>

		<p id='register_link' class='small_text'>Dont have an account? Click here to register</p>

	</div>


	<div class='register_form'>
		<h1>REGISTER</h1>
		<p>Register to create your accounts</p>
		<div class="form-box" id="login-box">
		
		<?php if(isset($model->errors)){ ?>
		<?php foreach ($model->errors as $error) { ?>
		<div class="alert alert-danger"><?php echo $error; ?></div>
		<?php } ?>
		<?php } ?>
		<?php if(isset($model->message)){ ?>
		<p><?php echo $model->message; ?></p>
		<?php } ?>
	<!--	<div class="header">ADMIN</div>-->
		<form id="admin_login" action="<?= ADMIN_URL ?>login" method="post">
			<div class="body bg-gray">
				<div class="form-group">
					<input type="text" name="username" class="form-control" placeholder="Username"/>
				</div>
				<div class="form-group">
					<input type="text" name="email" class="form-control" placeholder="Email"/>
				</div>
				<div class="form-group">
					<input type="text" name="phone" class="form-control" placeholder="Phone Number"/>
				</div>
				<div class="form-group">
					<input type="password" name="password" class="form-control" placeholder="Password"/>
				</div>
				<div class="form-group">
					<button type="submit" class="btn bg-black btn-block btn-lg button_border">Register</button>
				</div>

			</div>
		</form>
		</div>

		<p id='login_link' class='small_text'>Already have an account? Click here to login</p>

	</div>
	
</section>

<?php echo footer(); ?>

<script type="text/javascript">
	$("#admin_login").validate({
		rules: {
			username:{
				required: true,
			},
			password:{
				required: true,
			}
		},
		messages:{
			username:{ 
				required:"Please enter your username",
			},
			password:{ 
				required:"Please enter your password",
			}
		},
		errorClass: "error"
	}
	)
</script>