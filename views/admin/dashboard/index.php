<?php 
	$total_clients = $model->client;
	$total_images = $model->image;
	$total_cases = $model->project;
	$total_complete = $model->complete_image;
	$incomplete_images = $total_images - $total_complete;

?>

<div id="page-wrapper" class='dashboard_page'>
    <h1>Dashboard</h1>
    <section class='cards'>
    	<div class='card card_four'>
    		<img src="<?=FRONT_ASSETS?>img/client_dash.png">
    		<? if($total_clients > 0) { ?>
    			<p class='big_text'><a href="<?php echo ADMIN_URL; ?>clients"><?= $total_clients ?></a></p>
    		<? } else { ?>
    			<p class='big_text'><a href="<?php echo ADMIN_URL; ?>clients">0</a></p>
    		<? } ?>
    		<p class='card_title'><a href="<?php echo ADMIN_URL; ?>clients">Clients</a></p>
    	</div>
    	<div class='card card_four'>
    		<img src="<?=FRONT_ASSETS?>img/img_dash.png">
    		<? if($total_images > 0) { ?>
    			<p class='big_text'><?= $total_images ?></p>
    		<? } else { ?>
    			<p class='big_text'>0</p>
    		<? } ?>
    		<p class='card_title'>Images</p>
    	</div>
    	<div class='card card_four'>
    		<img src="<?=FRONT_ASSETS?>img/case_dash.png">
    		<? if($total_cases > 0) { ?>
    			<p class='big_text'><a href="<?php echo ADMIN_URL; ?>projects"><?= $total_cases ?></a></p>
    		<? } else { ?>
    			<p class='big_text'><a href="<?php echo ADMIN_URL; ?>projects">0</a></p>
    		<? } ?>
    		<p class='card_title'><a href="<?php echo ADMIN_URL; ?>projects">Projects</a></p>
    	</div>
    	<div class='card card_four'>
    		<img src="<?=FRONT_ASSETS?>img/no_image_dash.png">
    		<p class='big_text'><?= $incomplete_images ?></p>
    		<p class='card_title'>Incomplete Images</p>
    	</div>

    	<!-- Showing List of Clients -->
    	<div class='card card_one announcements'>
    		<p class='card_title'>Clients</p>
    		<div class='announcement_holder'>
    			<? foreach($model->client_lists as $client_list) {  
    				$client_total_images = \Model\Project_Image::getCount(['where'=>"active = 1 and client_id = $client_list->id"]);
    				$client_completed_images = \Model\Project_Image::getCount(['where'=>"active = 1 and client_id = $client_list->id and status = 1"]); 
    			?>
    			<div class='announcement'>
    				<p><?= $client_list->name ?></p>
    				<p class='card_title'><?php echo date('M d,Y',strtotime($client_list->insert_time)) ?></p>
    				<p><?= $client_completed_images ?>/<?= $client_total_images ?> Completed</p>
    			</div>
    			<? } ?>
    		</div>
    	</div>

    	<div class='card card_one announcements'>
    		<p class='card_title'>Announcements</p>
    		<div class='announcement_holder'>
	    		<? if(count($model->announcements) > 0){ 
	    			foreach($model->announcements as $announcement){ ?>
		    			<div class='announcement'>
		    				<p><?= $announcement->title ?></p>
		    				<p class='card_title'><?php echo date('M d, Y', strtotime($announcement->insert_time)) ?></p>
		    				<p><?= $announcement->description ?></p>
		    			</div>
		    		<? }
	    		 } else { ?>
	    		 	<p>There are no announcements yet.</p>
	    		<? } ?>
    		</div>
    	</div>

    	<div class='card scroll_card'>
    		<div class='sidebar_head'>
		       <p>RECENT ACTIVITY</p>
		     </div>
    		<div class='activities'>
    			<div class='activity'>
    				<p><span>User1</span> - has logged in.</p>
    			</div>
    			<div class='activity'>
    				<p><span>User2</span> - has added 3 images to project1.</p>
    			</div>
				<div class='activity'>
    				<p><span>User3</span> - has created an account.</p>
    			</div>
				<div class='activity'>
    				<p><span>User1</span> - has logged in.</p>
    			</div>
				<div class='activity'>
    				<p><span>User2</span> - has added 3 images to project1.</p>
    			</div>
				<div class='activity'>
    				<p><span>User3</span> - has created an account.</p>
    			</div>
    			</div>
    		</div>
    	</div>
    </section>

</div>

<script>


	var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
	var randomScalingFactor = function(){ return Math.round(Math.random()*100)};
	var dataSet = {"previous":{"2014-04":{"monthly_sales":null,"gross_margin":0,"monthly_orders":"0"},"2014-05":{"monthly_sales":null,"gross_margin":0,"monthly_orders":"0"},"2014-06":{"monthly_sales":null,"gross_margin":0,"monthly_orders":"0"},"2014-07":{"monthly_sales":null,"gross_margin":0,"monthly_orders":"0"},"2014-08":{"monthly_sales":null,"gross_margin":0,"monthly_orders":"0"},"2014-09":{"monthly_sales":null,"gross_margin":0,"monthly_orders":"0"},"2014-10":{"monthly_sales":null,"gross_margin":0,"monthly_orders":"0"}},"current":{"2015-04":{"monthly_sales":null,"gross_margin":0,"monthly_orders":"0"},"2015-05":{"monthly_sales":"4078.22","gross_margin":0,"monthly_orders":"8"},"2015-06":{"monthly_sales":"9485.6","gross_margin":0,"monthly_orders":"25"},"2015-07":{"monthly_sales":null,"gross_margin":0,"monthly_orders":"0"},"2015-08":{"monthly_sales":null,"gross_margin":0,"monthly_orders":"0"},"2015-09":{"monthly_sales":"35","gross_margin":0,"monthly_orders":"1"},"2015-10":{"monthly_sales":null,"gross_margin":0,"monthly_orders":"0"}}};

	var months = [];
	for (key in dataSet.previous){
		var month = new Date(key.split('-')[0], key.split('-')[1]-1);
		months.push(monthNames[month.getMonth()]);
	}

	var set = [[],[]];
	for (key in dataSet.previous){
		set[0].push(dataSet.previous[key].monthly_sales);
	}
	for (key in dataSet.current){
		set[1].push(dataSet.current[key].monthly_sales);
	}
	var barChartData1 = {
		labels : months,
		datasets : [
			{
				fillColor : "rgba(220,220,220,0.5)",
				strokeColor : "rgba(220,220,220,0.8)",
				highlightFill: "rgba(220,220,220,0.75)",
				highlightStroke: "rgba(220,220,220,1)",
				data : set[0]
			},
			{
				fillColor : "rgba(151,187,205,0.5)",
				strokeColor : "rgba(151,187,205,0.8)",
				highlightFill : "rgba(151,187,205,0.75)",
				highlightStroke : "rgba(151,187,205,1)",
				data : set[1]
			}
		]

	}

	var lineChartData = {
		labels : months,
		datasets : [
			{
				label: "Previous",
				fillColor : "rgba(220,220,220,0.2)",
				strokeColor : "rgba(220,220,220,1)",
				pointColor : "rgba(220,220,220,1)",
				pointStrokeColor : "#fff",
				pointHighlightFill : "#fff",
				pointHighlightStroke : "rgba(220,220,220,1)",
				data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
			},
			{
				label: "Current",
				fillColor : "rgba(151,187,205,0.2)",
				strokeColor : "rgba(151,187,205,1)",
				pointColor : "rgba(151,187,205,1)",
				pointStrokeColor : "#fff",
				pointHighlightFill : "#fff",
				pointHighlightStroke : "rgba(151,187,205,1)",
				data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
			}
		]

	}

	set = [[],[]];
	for (key in dataSet.previous){
		set[0].push(dataSet.previous[key].monthly_orders);
	}
	for (key in dataSet.current){
		set[1].push(dataSet.current[key].monthly_orders);
	}
	var barChartData2 = {
		labels : months,
		datasets : [
			{
				fillColor : "rgba(220,220,220,0.5)",
				strokeColor : "rgba(220,220,220,0.8)",
				highlightFill: "rgba(220,220,220,0.75)",
				highlightStroke: "rgba(220,220,220,1)",
				data : set[0]
			},
			{
				fillColor : "rgba(151,187,205,0.5)",
				strokeColor : "rgba(151,187,205,0.8)",
				highlightFill : "rgba(151,187,205,0.75)",
				highlightStroke : "rgba(151,187,205,1)",
				data : set[1]
			}
		]

	}

	window.onload = function(){
		var ctxB = document.getElementById("B").getContext("2d");
	 	window.myLine = new Chart(ctxB).Line(lineChartData, {
	 		responsive: true
	 	});
		var ctxA = document.getElementById("A").getContext("2d");
		window.myBar = new Chart(ctxA).Bar(barChartData1, {
			responsive : true
		});
		var ctxC = document.getElementById("C").getContext("2d");
		window.myBar = new Chart(ctxC).Bar(barChartData2, {
			responsive : true
		});
	}

</script> 
 
<?php echo footer(); ?>