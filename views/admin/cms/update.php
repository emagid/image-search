<div id="page-wrapper" class='clients_page cms_update'>
	<p class='breadcrumb'>CMS / </p>
    <h1>PAGE TITLE</h1>
	<section class='table_card'>
		<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
		  	<div role="tabpanel">
			    <div class="tab-content">
			      	<div role="tabpanel" class="tab-pane active" id="test">
				        <input type="hidden" name="id" value="<?php echo $model->client->id; ?>" />
				        <input name="token" type="hidden" value="<?php echo get_token();?>" />
				        <div class="row">
				            <div class="col-md-24">
				            	<div class='box'>
				                    <h4>PAGE CONTENT</h4>

				                    <div class="form-group">
				                        <label class='card_title'>Content</label>
				                        <textarea style='max-width: 100%;' type="text" name=""></textarea>
				                    </div>
									  <button type="submit" class="button">Save</button>
				                </div>
				            </div>
				        </div>
				    </div>
			    </div>
		  	</div>
		</form>
	</section>
</div>

<?php echo footer(); ?>
 