<div id="page-wrapper" class='clients_page'>
	<h1>Queue</h1>

	<div class="filters project_text">
		<div class="custom-select" style="width:200px;">
		  <select>
		    <option value="0">Change View</option>
		    <option value="2">Infringement View</option>
		    <option value="3">Image View</option>
		  </select>
		</div>
		<div>
			<input type="checkbox" style="margin: 0;"> Select All
		</div>
		<div style="margin-left: 15px;">
			  
		</div>
	</div>


	<section class='cards image_view'>
    	<?php if(count($model->queue) > 0) { ?>
			<? foreach($model->queue as $queue) { 
				$casedetail = \Model\Project::getItem(null,['where'=>"id = $queue->project_id"]); ?>
				<a href="/admin/projects/image/<?=$queue->id ?>"><div class="table_card img_card">
					<div class="name">
						<? $img_path = UPLOAD_URL.'projects/'.$queue->image;  ?>
						<div class="image" style="background-image: url(<?php echo $img_path; ?>);">	
						</div>
					</div>
					<div class="image_info">
						<div>
							<p class="image_text img_client"><?php echo $casedetail->name ?></p>
							<p class='image_text img_path'>image pathway</p>
							<!-- <p class="image_text img_client"><?php echo date('M d,Y',strtotime($queue->insert_time)) ?></p> -->
						</div>
						
					</div>
				</div></a>
			<? } ?>
		<? } else { ?>
					<div class='table_card'>
						<p>You currently have no images</p>
					</div>
		    <? } ?>  	
    </section>

    <section class='cards infringe_view'>
    	<?php if(count($model->queue) > 0) { ?>
			<? foreach($model->queue as $queue) { 
				$casedetail = \Model\Project::getItem(null,['where'=>"id = $queue->project_id"]); ?>
				<a href="/admin/projects/image"><div class="table_card img_card">
					<div class="name">
						<? $img_path = UPLOAD_URL.'projects/'.$queue->image;  ?>
						<div class="image" style="background-image: url(<?php echo $img_path; ?>);">	
						</div>
					</div>
					<div class="image_info">
						<div>
							<p class="image_text img_client"><?php echo $casedetail->name ?></p>
							<!-- <p class="image_text img_client"><?php echo date('M d,Y',strtotime($queue->insert_time)) ?></p> -->
							<div class='image url_img' style="background-image: url(<?php echo $img_path; ?>);"></div><p class='image_text img_path'>Infringmenturl/locationlocation/location.com</p>
						</div>
						
					</div>
					<div class='infringe_num'>5</div>
				</div></a>
			<? } ?>
		<? } else { ?>
					<div class='table_card'>
						<p>You currently have no images</p>
					</div>
		    <? } ?>  	
    </section>

	<!-- <section class="cards">
		<?php if(count($model->queue) > 0) { ?>
			<? foreach($model->queue as $queue) { 
				$casedetail = \Model\Project::getItem(null,['where'=>"id = $queue->project_id"]); ?>
				<div class="table_card img_card">
					<div class="name">
						<? $img_path = UPLOAD_URL.'projects/'.$queue->image;  ?>
						<div class="image" style="background-image: url(<?php echo $img_path; ?>);">	
						</div>
					</div>
					<div class="image_info">
						<div>
							<p class="image_text img_client"><?php echo $casedetail->name ?></p>
							<p class="image_text img_client"><?php echo date('M d,Y',strtotime($queue->insert_time)) ?></p>
						</div>
						
					</div>
				</div>
			<? } ?>
		<? } ?>
	</section> -->
</div>


<script type="text/javascript">
	
	if( $('.select-selected').html('Image View') ) {
		$('.image_view').show();
		$('.infringe_view').hide();
	}else {
		$('.image_view').hide();
		$('.infringe_view').show();
	}

	$(document).on('click', '.select-items div:nth-child(2)', function(){
		$('.infringe_view').slideUp();
		$('.image_view').slideDown();
	});

	$(document).on('click', '.select-items div:nth-child(1)', function(){
		$('.image_view').slideUp();
		$('.infringe_view').slideDown();
	});

	


</script>


<?php  footer(); ?>