<?php 
	$c_detail = $model->client; ?>
<div id="page-wrapper" class='clients_page'>
    <h1>Clients</h1>
    <div class='table_card'>
        <form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
            <div role="tabpanel">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="general-tab">
                        <input type="hidden" name="id" value="<?php echo $model->project->id; ?>" />
                        <input name="token" type="hidden" value="<?php echo get_token();?>" />
                        <div class="row">
                            <div class="col-md-24">
                                <div class="box">
                                    <h4>Case Information</h4>
                                    
                                    <div class="form-group">
                                        <label>Name</label>
                                        <?php echo $model->form->editorFor("name"); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Description</label>
                                        <?php echo $model->form->editorFor(""); ?>
                                    </div>
                                    <div class="form-group user_assign">
                                        <label>User Assignment</label>
                                        <?php echo $model->form->editorFor(""); ?>
                                    </div>
                                    <div class='user_list'>
                                        <div class='list'>
                                            <p>User Name</p>
                                        </div>
                                        <div class='list'>
                                            <p>User Name</p>
                                        </div>
                                        <div class='list'>
                                            <p>User Name</p>
                                        </div>
                                        <div class='list'>
                                            <p>User Name</p>
                                        </div>
                                        <div class='list'>
                                            <p>User Name</p>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label>Case Status</label>
                                        <?php echo $model->form->editorFor(""); ?>
                                    </div>
                                    <div class="form-group">    
                                        <input type="hidden" name="client_id" value="<?php echo $c_detail->id; ?>" />
                                    </div>
                                <button type="submit" class="button">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- <div id="target" class="btn btn-save">Save</div> -->
            <!-- <button type="submit" class="btn btn-save">Save</button> -->
        </form>
    </div>
</div>


<script type="text/javascript">
    $('.user_assign input').on('input', function(){
        $('.user_list').slideDown();
      });

      $(document).mouseover(function(e) {
        var target = e.target;

        if (!$(target).is('.user_assign, .user_list') && !$(target).parents().is('.user_assign, .user_list')) {
            $('.user_list').fadeOut();
        }
      });
</script>

<?php echo footer(); ?>


