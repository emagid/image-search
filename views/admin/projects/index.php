<div id="page-wrapper" class='clients_page projects_page'>
    <h1>Cases</h1>	
	<?php if(count($model->projects) > 0) { ?>
		    <? foreach($model->projects as $project) { 
		    	$imageSql = "Select * from project_image where project_id = ".$project->id;
		    	$totalImage = \Model\Project_Image::getCount(['where'=>"project_id = $project->id"]);
		    	$completeImage = \Model\Project_Image::getCount(['where'=>"project_id = $project->id and status = 1"]);
		    	?>
		    	<a href= "<?php echo ADMIN_URL; ?>projects/project/<?= $project->id ?>">
			    	<div class='table_card table_row card_four client_card'>
			    		<div class='name'>
				    		<h4><?=$project->name ?></h4>
				    		<p class='card_title'><?php echo date('M d, Y',strtotime($project->insert_time)) ?></p>
			    		</div>

			    		<div class='client_info'>
			    			<div>
					    		<p class='big_text'><?= $completeImage ?> <span>/</span> <?= $totalImage ?></p>
					    		<p class='card_title'>Completed</p>
			    			</div>
			    		</div>
			    	</div>
		    	</a>
			<? } ?>	
	<?php } else { ?>
			<section class='table_cards'>
				<div class='table_card'>
					<p>You currently have no Cases</p>
				</div>
			</section>
	<?php } ?>
</div>

<script type="text/javascript">
	$(document).ready(function(){
	    $('.table_row').mouseover(function(){
	        $(this).addClass('scale');
	    });

	    $('.table_row').mouseleave(function(){
	        $(this).removeClass('scale');
	    });
	});

</script>
