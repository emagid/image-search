<?php 
$clientcase = $model->project;
?>
<div id="page-wrapper" class='clients_page case_page'>
	<p class='breadcrumb'>Client Name / Projects / </p>
    <h1><?= $clientcase->name ?></h1>
	<a class='button loose' href="<?php echo ADMIN_URL; ?>projects/update_image/<?= $clientcase->id ?>">Add Images&nbsp; <i class="icon-plus"></i></a>



	
	<div class="filters project_text">
		<div class="custom-select" style="width:200px;">
		  <select>
		    <option value="0">Change View</option>
		    <option value="2">Infringement View</option>
		    <option value="3">Image View</option>
		  </select>
		</div>
		<div>
			<input type="checkbox" style="margin: 0;"> Select All
		</div>
		<div style="margin-left: 15px;">
			  
		</div>
	</div>
    <section class='cards image_view'>
    	<?php if(count($model->case_images) > 0){ 
    		foreach($model->case_images as $case_image){
    			$client_info = \Model\Client::getItem(null,['where'=>"id = $case_image->client_id"]); 
    	?>	
    			<a href="/admin/projects/image/<?=$case_image->id?>"><div class='table_card img_card'>
		    		<div class='name'>
		    			<?$img_path = UPLOAD_URL.'projects/'.$case_image->image?>
		    			<div class='image' style="background-image: url(<?php echo $img_path; ?>);"></div>
		    			<!-- <img src="<?php echo $img_path; ?>" width="50"/> -->
			    		
		    		</div>
		    		<div class='image_info'>
		    			<div>
				    		<!-- <p class='image_text img_client'><?= $client_info->name ?></p> -->
				    		<p class='image_text img_client'>Image name</p>
				    		<p class='image_text img_path'>image pathway</p>
				    		<!-- <p class='card_title'>Client</p> -->
		    			</div>
		    			<div>
				    		<!-- <p class='image_text'><?= $clientcase->name ?></p> -->
				    		<!-- <p class='card_title'>Case</p> -->
		    			</div>
		    		</div>

		    		<div class='infringe_num'>5</div>
		    	</div></a>

    		<? } 
    	} else { ?>  
					<div class='table_card'>
						<p>You currently have no images</p>
					</div>
		    <? } ?>  	
    </section>

    <section class='cards infringe_view'>
    	<?php if(count($model->case_images) > 0){ 
    		foreach($model->case_images as $case_image){
    			$client_info = \Model\Client::getItem(null,['where'=>"id = $case_image->client_id"]); 
    	?>	
    			<a href="/admin/projects/image"><div class='table_card img_card'>
		    		<div class='name'>
		    			<?$img_path = UPLOAD_URL.'projects/'.$case_image->image?>
		    			<div class='image' style="background-image: url(<?php echo $img_path; ?>);"></div>
		    			<!-- <img src="<?php echo $img_path; ?>" width="50"/> -->
			    		
		    		</div>
		    		<div class='image_info'>
		    			<div>
				    		<!-- <p class='image_text img_client'><?= $client_info->name ?></p> -->
				    		<p class='image_text img_client'>Image name</p>
				    		<div class='image url_img' style="background-image: url(<?php echo $img_path; ?>);"></div><p class='image_text img_path'>Infringmenturl/locationlocation/location.com</p>
				    		<!-- <p class='card_title'>Client</p> -->
		    			</div>
		    			<div>
				    		<!-- <p class='image_text'><?= $clientcase->name ?></p> -->
				    		<!-- <p class='card_title'>Case</p> -->
		    			</div>
		    		</div>

		    		<div class='infringe_num'>5</div>
		    	</div></a>

    		<? } 
    	} else { ?>  
					<div class='table_card'>
						<p>You currently have no images</p>
					</div>
		    <? } ?>  	
    </section>
</div>


<script type="text/javascript">
	
	if( $('.select-selected').html('Image View') ) {
		$('.image_view').show();
		$('.infringe_view').hide();
	}else {
		$('.image_view').hide();
		$('.infringe_view').show();
	}

	$(document).on('click', '.select-items div:nth-child(2)', function(){
		$('.infringe_view').slideUp();
		$('.image_view').slideDown();
	});

	$(document).on('click', '.select-items div:nth-child(1)', function(){
		$('.image_view').slideUp();
		$('.infringe_view').slideDown();
	});

	


</script>
<?php echo footer(); ?>


