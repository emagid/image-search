<!-- Google Drive Integration -->
<script type="text/javascript">
  var clientId = '45746481863-j3ll2bhl0ofdod06nv7649icqe3hsaem.apps.googleusercontent.com';
  var developerKey = 'AIzaSyC6gmnC7oCSk7csDRxneymYZxefLFf6NyE';
  var appId = '45746481863';
  var scope = ['https://www.googleapis.com/auth/drive'];
  var pickerApiLoaded = false;
  var oauthToken;

    function loadPicker() {
      gapi.load('auth', {'callback': onAuthApiLoad});
      gapi.load('picker', {'callback': onPickerApiLoad});
    }

    function onAuthApiLoad() {
        var authBtn = document.getElementById('google-drive-upload');
        authBtn.addEventListener('click', function(){
            window.gapi.auth.authorize({
                'client_id': clientId,
                'scope': scope,
                'immediate': false
            }, handleAuthResult);
        });
    }

    function onPickerApiLoad() {
        pickerApiLoaded = true;
        createPicker();
    }

    function handleAuthResult(authResult) {
        if(authResult && !authResult.error) {
            oauthToken = authResult.access_token;
            createPicker();
        }else {
            //Write the code here if there is an error authenticating the user.
        }
    }

    function createPicker() {
        if (pickerApiLoaded && oauthToken) {
            var view = new google.picker.View(google.picker.ViewId.DOCS);
            view.setMimeTypes("image/png,image/jpeg,image/jpg");
            var picker = new google.picker.PickerBuilder()
                .enableFeature(google.picker.Feature.NAV_HIDDEN)
                .enableFeature(google.picker.Feature.MULTISELECT_ENABLED)
                .setAppId(appId)
                .setOAuthToken(oauthToken)
                .addView(view)
                .addView(new google.picker.DocsUploadView())
                .setDeveloperKey(developerKey)
                .setCallback(pickerCallback)
                .build();
            picker.setVisible(true);
        }
    }

    function pickerCallback(data) {
      if (data.action == google.picker.Action.PICKED) {
        var fileId = data.docs[0].id;
        alert('The user selected: ' + fileId);
      }
    }

</script>



<?php 
    $pcase = $model->pcases;
?>

<div id="page-wrapper" class='clients_page'>
    <h1>Image Uploads</h1>
    <div class='table_card'>
        <form class="form" method="post" enctype="multipart/form-data" >
          	<div role="tabpanel">
        	    <div class="tab-content">
        	      	<div role="tabpanel" class="tab-pane active" id="general-tab">
        		        <input name="token" type="hidden" value="<?php echo get_token();?>" />
                        <div class="row">
                            <div class="col-md-24">
                                <div class="box">
                                	<div class="form-group">
        		                        <label>Name</label>
        		                        <!-- <?//php echo $model->form->editorFor("name"); ?> -->
                                        <input type="text" name="name" value="<?php echo $pcase->name; ?>" />
        		                    </div>
                                    <div class="dropzone" id="dropzoneForm"
                                         action="<?php echo ADMIN_URL . 'projects/upload_images/'. $pcase->id; ?>">
                                    </div>
                                    <button id="upload-dropzone" class="button">Upload All Images</button>
                                    <div class='or'><p>OR</p></div>

                                    <div class='img_options'>
                                        <div class='img_option'>
                                            <button type="button" id="google-drive-upload"><img src="<?=FRONT_ASSETS?>img/google_drive.jpeg"></button>
                                            <p>Upload from Google Drive</p>
                                        </div>

                                        <div class='img_option'>
                                            <div id="dropbox-container">
                                                <img src="<?=FRONT_ASSETS?>img/dropbox.png">
                                                <p>Upload from Dropbox</p>
                                            </div>
                                            <input id="dropbox_image" name="image" type="hidden" />
                                        </div>
                                    </div>

                                    <!-- <a class='button' style='margin-top: 50px; left: 50%; position: absolute; transform: translateX(-50%);' href="/admin/projects/image_search">TEST RESULTS</a> -->

                                    <br/>
                                    <ul id="img_list">
                                    </ul>
        							<div class="form-group">	
        		                        <input type="hidden" name="client_id" value="<?php echo $pcase->client_id; ?>" />
        		                    </div>
        		                    <div class="form-group">	
        		                        <input type="hidden" name="project_id" value="<?php echo $pcase->id; ?>" />
        		                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-24">
                                <div class="prev_images">
                                    <table id="image-container" class="table table-sortable-container">
                                        <thead>
                                        <tr>
                                            <th>Image</th>
                                            <th>File Name</th>
                                            <th>Delete</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach (\Model\Project_Image::getList(['where' => 'project_id=' . $pcase->id, 'sort' => 'DESC']) as $pimg) {

                                            if ($pimg->exists_image()) {
                                                ?>
                                                <tr data-image_id="<?php echo $pimg->id; ?>">
                                                    <td><img src="<?php echo $pimg->get_image_url(); ?>" width="100"
                                                             height="100"/></td>
                                                    <td><?php echo $pimg->image; ?></td>
                                                    
                                                    <td class="text-center">
                                                        <a class="btn-actions delete-product-image"
                                                           href="<?php echo ADMIN_URL; ?>projects/delete_proj_image/<?php echo $pimg->id; ?>?token_id=<?php echo get_token(); ?>">
                                                            <i class="icon-cancel-circled"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        } ?>
                                        </tbody>
                                    </table>
                              	<button type="submit" class="button save_button">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- <div id="target" class="btn btn-save">Save</div> -->
          	<!-- <button type="submit" class="btn btn-save">Save</button> -->
        </form>
    </div>
</div>
<script type="text/javascript" src="https://apis.google.com/js/api.js?onload=loadPicker"></script>
<!-- <script type="text/javascript" src="https://apis.google.com/js/api.js?onload=onApiLoad"></script> -->

<?php echo footer(); ?>
<!-- <script src="dropzone.min.js"></script> -->
<script type="text/javascript">
    Dropzone.options.dropzoneForm = { // The camelized version of the ID of the form element

        // The configuration we've talked about above
        autoProcessQueue: false,
        uploadMultiple: true,
        parallelUploads: 100,
        maxFiles: 100,
        url: <?php  echo json_encode(ADMIN_URL.'projects/upload_images/'.$pcase->id);?>,
        // The setting up of the dropzone
        init: function () {
            var myDropzone = this;

            // First change the button to actually tell Dropzone to process the queue.
            $("#upload-dropzone").on("click", function (e) {
                // Make sure that the form isn't actually being sent.
                e.preventDefault();
                e.stopPropagation();
                myDropzone.processQueue();
            });

            // Listen to the sendingmultiple event. In this case, it's the sendingmultiple event instead
            // of the sending event because uploadMultiple is set to true.
            this.on("sendingmultiple", function () {
                // Gets triggered when the form is actually being sent.
                // Hide the success button or the complete form.
                $("#upload-dropzone").prop("disabled", true);
            });
            this.on("successmultiple", function (files, response) {
                // console.log(response);
                // Gets triggered when the files have successfully been sent.
                // Redirect user or notify of success.
                
                $("#upload-dropzone").prop("disabled", false);
            });
            this.on("errormultiple", function (files, response) {
                // Gets triggered when there was an error sending the files.
                // Maybe show form again, and notify user of error
                // console.log(response);
            });
        }

    }
</script>





<!-- Dropbox API integration -->
<script type="text/javascript" src="https://www.dropbox.com/static/api/2/dropins.js" id="dropboxjs" data-app-key="wgiv8kvzvq57mlw"></script>
<script type="text/javascript">
    options = {
        success: function(files) {
            files.forEach(function(file) {
                add_img_to_list(file);
                document.getElementById('dropbox_image').value = file['name'];
                console.log(file);
            });
        },
        cancel: function() {
          //optional
        },
        linkType: "direct", // "preview" or "direct"
        multiselect: true, // true or false
        extensions: ['.png', '.jpg', '.jpeg'],

    };

    var button = Dropbox.createChooseButton(options);
    document.getElementById("dropbox-container").appendChild(button);

    function add_img_to_list(file) {
        var li  = document.createElement('li');
        var a   = document.createElement('a');
        a.href = file.link;
        var img = new Image();
        var src = file.thumbnailLink;
        src = src.replace("bounding_box=75", "bounding_box=256");
        src = src.replace("mode=fit", "mode=crop");
        img.src = src;
        img.className = "th"
        document.getElementById("img_list").appendChild(li).appendChild(a).appendChild(img);
    }


    
</script>

<!-- <script>
    $('.save_button').click(function(){
        $.POST()
    })
</script> -->

