<div id="page-wrapper" class='clients_page'>
	<h1>Reports</h1>
	<section class="filters">
		<div class="custom-select" style="width: 200px;">
			<select>
				<option value="0">Search for..</option>
				<option value="">Clients</option>
				<option value="">Cases</option>
				<option value="">Images</option>
			</select>
		</div>
		<div class="custom-select date" style="width: 100px;">
			<p class='date_range'>Date Range</p>
			<select>
				<option value="0">From</option>
			</select>
		</div>
		<div class="custom-select date" style="width: 100px;">
			<select>
				<option value="0">To</option>
			</select>
		</div>
	</section>

	<section class='cards image_view'>
		<a href="/admin/projects/image"><div class="table_card img_card report_card">
			<div class="image_info">
				<div>
					<p class="image_text img_client">CLIENT NAME</p>
					<p class='image_text img_path'>image pathway</p>
					<!-- <p class="image_text img_client"><?php echo date('M d,Y',strtotime($queue->insert_time)) ?></p> -->
				</div>
				
			</div>
		</div></a>
		<a href="/admin/projects/image"><div class="table_card img_card report_card">
			<div class="image_info">
				<div>
					<p class="image_text img_client">CLIENT NAME</p>
					<p class='image_text img_path'>image pathway</p>
					<!-- <p class="image_text img_client"><?php echo date('M d,Y',strtotime($queue->insert_time)) ?></p> -->
				</div>
				
			</div>
		</div></a>
		<a href="/admin/projects/image"><div class="table_card img_card report_card">
			<div class="image_info">
				<div>
					<p class="image_text img_client">CLIENT NAME</p>
					<p class='image_text img_path'>image pathway</p>
					<!-- <p class="image_text img_client"><?php echo date('M d,Y',strtotime($queue->insert_time)) ?></p> -->
				</div>
				
			</div>
		</div></a>
		<a href="/admin/projects/image"><div class="table_card img_card report_card">
			<div class="image_info">
				<div>
					<p class="image_text img_client">CLIENT NAME</p>
					<p class='image_text img_path'>image pathway</p>
					<!-- <p class="image_text img_client"><?php echo date('M d,Y',strtotime($queue->insert_time)) ?></p> -->
				</div>
				
			</div>
		</div></a>
	</section>
</div>

<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'reports/index';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>
