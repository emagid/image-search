<div id="page-wrapper" class='clients_page'>
    <h1>Announcement</h1>
	<section class='table_card'>
		<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
		  	<div role="tabpanel">
			    <div class="tab-content">
			      	<div role="tabpanel" class="tab-pane active" id="test">
				        <input type="hidden" name="id" value="<?php echo $model->announcement->id; ?>" />
				        <input name="token" type="hidden" value="<?php echo get_token();?>" />
				        <div class="row">
				            <div class="col-md-24">
				            	<div class='box'>
				                    <h4>New Announcement</h4>
				                    
				                    <div class="form-group">
				                        <label class='card_title'>Announcement Title</label>
				                        <?php echo $model->form->editorFor("title"); ?>
				                    </div>
				                    <div class="form-group">
				                        <label class='card_title'>Description</label>
				                        <?php echo $model->form->editorFor("description"); ?>
				                    </div>
									  <button type="submit" class="button">Save</button>
				                </div>
				            </div>
				        </div>
				    </div>
			    </div>
		  	</div>
		</form>
	</section>
</div>

<?php echo footer(); ?>
 