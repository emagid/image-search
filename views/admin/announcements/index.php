<?php
 if(count($model->announcements)>0){ ?>
  <div class="box box-table">
    <table class="table">
      <thead>
        <tr>
            <th width="10%">Id</th>
            <th width="20%">Announcement Title</th>
            <th width="15%" class="text-center">Edit</th>
            <th width="15%" class="text-center">Delete</th>	
        </tr>
      </thead>
      <tbody>
       <?php foreach($model->announcements as $obj){ ?>
        <tr>
          <td><a href="<?php echo ADMIN_URL; ?>announcements/update/<?= $obj->id ?>"><?php echo $obj->id; ?></a></td>
          <td><a href="<?php echo ADMIN_URL; ?>announcements/update/<?= $obj->id ?>"><?php echo $obj->title; ?></a></td>  
          <td class="text-center">
            <a class="btn-actions" href="<?= ADMIN_URL ?>announcements/update/<?= $obj->id ?>">
              <i class="icon-pencil"></i> 
            </a>
          </td>
          <td class="text-center">
            <a class="btn-actions" href="<?= ADMIN_URL ?>announcements/delete/<?= $obj->id ?>?token_id=<?php echo get_token();?>" onClick="return confirm('Are You Sure?');">
              <i class="icon-cancel-circled"></i> 
            </a>
          </td>
        </tr>
        <?php } ?>
    </tbody>
  </table>
  <div class="box-footer clearfix">
    <div class='paginationContent'></div>
  </div>
</div>
<?php } else { ?>
  <section class='table_cards'>
    <div class='table_card'>
      <p>You currently have no Announcements to display.</p>
    </div>
  </section>
<? } ?>

<?php echo footer(); ?>
<script type="text/javascript">
	var site_url = '<?= ADMIN_URL.'announcements';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>