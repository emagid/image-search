<div id="page-wrapper" class='clients_page'>
	<h1>Image Queue</h1>


	<section class='cards image_view'>
    	<?php if(count($model->queue) > 0) { ?>
			<? foreach($model->queue as $queue) { 
				$casedetail = \Model\Project::getItem(null,['where'=>"id = $queue->project_id"]); ?>
				<a href="/admin/image_queue/image"><div class="table_card img_card">
					<div class="name">
						<? $img_path = UPLOAD_URL.'projects/'.$queue->image;  ?>
						<div class="image" style="background-image: url(<?php echo $img_path; ?>);">	
						</div>
					</div>
					<div class="image_info">
						<div>
							<p class="image_text img_client"><?php echo $casedetail->name ?></p>
							<p class='image_text img_path'>image pathway</p>
							<!-- <p class="image_text img_client"><?php echo date('M d,Y',strtotime($queue->insert_time)) ?></p> -->
						</div>
						
					</div>

					<div class='right_tools'>
						<img style='height: 33px;' src="<?= FRONT_ASSETS ?>/img/check_dark.png">
						<img src="<?= FRONT_ASSETS ?>/img/delete.png">
					</div>
				</div></a>
			<? } ?>
		<? } else { ?>
					<div class='table_card'>
						<p>You currently have no images</p>
					</div>
		    <? } ?>  	
    </section>



	<!-- <section class="cards">
		<?php if(count($model->queue) > 0) { ?>
			<? foreach($model->queue as $queue) { 
				$casedetail = \Model\Project::getItem(null,['where'=>"id = $queue->project_id"]); ?>
				<div class="table_card img_card">
					<div class="name">
						<? $img_path = UPLOAD_URL.'projects/'.$queue->image;  ?>
						<div class="image" style="background-image: url(<?php echo $img_path; ?>);">	
						</div>
					</div>
					<div class="image_info">
						<div>
							<p class="image_text img_client"><?php echo $casedetail->name ?></p>
							<p class="image_text img_client"><?php echo date('M d,Y',strtotime($queue->insert_time)) ?></p>
						</div>
						
					</div>
				</div>
			<? } ?>
		<? } ?>
	</section> -->
</div>


<script type="text/javascript">
	
	if( $('.select-selected').html('Image View') ) {
		$('.image_view').show();
		$('.infringe_view').hide();
	}else {
		$('.image_view').hide();
		$('.infringe_view').show();
	}

	$(document).on('click', '.select-items div:nth-child(2)', function(){
		$('.infringe_view').slideUp();
		$('.image_view').slideDown();
	});

	$(document).on('click', '.select-items div:nth-child(1)', function(){
		$('.image_view').slideUp();
		$('.infringe_view').slideDown();
	});

	


</script>


<?php  footer(); ?>