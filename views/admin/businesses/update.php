<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
	<?php //echo $model->form->editorFor('id',[],'',['type'=>'hidden']);?>
  <div role="tabpanel">
    <ul class="nav nav-tabs" role="tablist">
      <li role="presentation" class="active"><a href="#test" aria-controls="general" role="tab" data-toggle="tab">General</a></li>
 
    </ul>
    
    <div class="tab-content">
      <div role="tabpanel" class="tab-pane active" id="test">
        <input type="hidden" name="id" value="<?php echo $model->business->id; ?>" />
        <input name="token" type="hidden" value="<?php echo get_token();?>" />
        <div class="row">
            <div class="col-md-24">
                <div class="box">
                    <h4>General</h4>
                    <div class="form-group">
                        <label>Logo</label>
                        <p><input type="file" name="logo" class='image'/></p>

                        <div style="display:inline-block">
                            <?php
                            $img_path = "";
                            if ($model->business->logo != "" && file_exists(UPLOAD_PATH . 'business' . DS . $model->business->logo)) {
                                $img_path = UPLOAD_URL . 'business/' . $model->business->logo;
                                ?>
                                <div class="well well-sm pull-left">
                                    <img src="<?php echo $img_path; ?>" width="100"/>
                                    <br/>
                                    <a href="<?= ADMIN_URL . 'business/delete_image/' . $model->business->id; ?>?logo=1"
                                       onclick="return confirm('Are you sure?');"
                                       class="btn btn-default btn-xs">Delete</a>
                                    <input type="hidden" name="logo"
                                           value="<?= $model->business->logo ?>"/>
                                </div>
                            <?php } else if($model->business->logo != "") {
                                $img_path = UPLOAD_URL .$model->business->logo; ?>
                                <div class="well well-sm pull-left">
                                    <img src="<?php echo $img_path; ?>" width="100"/>
                                    <br/>
                                    <a href="<?= ADMIN_URL . 'business/delete_image/' . $model->business->id; ?>?logo=1"
                                       onclick="return confirm('Are you sure?');"
                                       class="btn btn-default btn-xs">Delete</a>
                                    <input type="hidden" name="featured_image1"
                                           value="<?= $model->business->logo ?>"/>
                                </div>
                            <?}?>
                            <div class='preview-container'></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Name</label>
                        <?php echo $model->form->editorFor("name"); ?>
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <?php echo $model->form->editorFor("email"); ?>
                    </div>
                    <div class="form-group">
                        <label>Floor</label>
                        <?php echo $model->form->editorFor("floor"); ?>
                    </div>
                    <div class="form-group">
                        <label>Phone</label>
                        <?php echo $model->form->editorFor("phone"); ?>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>
  <button type="submit" class="btn btn-save">Save</button>
</form>

<?php echo footer(); ?>
 