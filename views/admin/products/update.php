<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->product->id; ?>"/>

    <div role="tabpanel">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#general-tab" aria-controls="general" role="tab"
                                                      data-toggle="tab">General</a></li>
            <li role="presentation"><a href="#seo-tab" aria-controls="seo" role="tab" data-toggle="tab">SEO</a></li>
            <li role="presentation"><a href="#categories-tab" aria-controls="categories" role="tab" data-toggle="tab">Categories</a>
            </li>

            <li role="presentation"><a href="#images-tab" aria-controls="images" role="tab" data-toggle="tab">Images</a>
            </li>
        </ul>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="general-tab">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <h4>General</h4>

                            <div class="form-group">
                                <label>Name</label>
                                <?php echo $model->form->editorFor("name"); ?>
                            </div>
                            <div class="form-group">
                                <label>Featured image</label>

                                <p>
                                    <small>(ideal featured image size is 500 x 300)</small>
                                </p>
                                <p><input type="file" name="featured_image" class='image'/></p>

                                <div style="display:inline-block">
                                    <?php
                                    $img_path = "";
                                    if ($model->product->featured_image != "" && file_exists(UPLOAD_PATH . 'products' . DS . $model->product->featured_image)) {
                                        $img_path = UPLOAD_URL . 'products/' . $model->product->featured_image;
                                        ?>
                                        <div class="well well-sm pull-left">
                                            <img src="<?php echo $img_path; ?>" width="100"/>
                                            <br/>
                                            <a href="<?= ADMIN_URL . 'products/delete_image/' . $model->product->id; ?>?featured_image=1"
                                               onclick="return confirm('Are you sure?');"
                                               class="btn btn-default btn-xs">Delete</a>
                                            <input type="hidden" name="featured_image"
                                                   value="<?= $model->product->featured_image ?>"/>
                                        </div>
                                    <?php } ?>
                                    <div class='preview-container'></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Price</label>
                                <?php echo $model->form->editorFor("price"); ?>
                            </div>
                            <div class="form-group">
                                <label>Description</label>
                                <?php echo $model->form->textAreaFor("description", ['class' => 'ckeditor']); ?>
                            </div>

                            <!-- <div class="form-group">
                      <label>Brand</label>
                      <select name="brand_id">
                      	<option value="0">Please select a brand</option>
                        <?php foreach ($model->brands as $b) {
                                $selected = ($b->id == $model->product->brand_id) ? " selected='selected'" : "";
                                ?>
                        	<option value="<?php echo $b->id; ?>"<?php echo $selected; ?>><?php echo $b->name; ?></option>
                      	<?php } ?>
                      </select>
                    </div> -->
                            <!-- <div class="form-group">
                      <label>Color</label>
                      <select name="color">
                      	<option value="0">Please select a color</option>
                      <?php foreach ($model->colors as $c) {
                                $selected = ($c->id == $model->product->color) ? " selected='selected'" : "";
                                ?>
                        <option value="<?php echo $c->id; ?>"<?php echo $selected; ?>><?php echo $c->name; ?></option>
                      <?php } ?>
                      </select>
                    </div> -->
                        </div>
                    </div>

                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="seo-tab">
                <div class="row">
                    <div class="col-md-24">
                        <div class="box">
                            <h4>SEO</h4>

                            <div class="form-group">
                                <label>Slug</label>
                                <?php echo $model->form->editorFor("slug"); ?>
                            </div>
                            <div class="form-group">
                                <label>Tags</label>
                                <?php echo $model->form->editorFor("tags"); ?>
                            </div>
                            <div class="form-group">
                                <label>Meta Title</label>
                                <?php echo $model->form->editorFor("meta_title"); ?>
                            </div>
                            <div class="form-group">
                                <label>Meta Keywords</label>
                                <?php echo $model->form->editorFor("meta_keywords"); ?>
                            </div>
                            <div class="form-group">
                                <label>Meta Description</label>
                                <?php echo $model->form->editorFor("meta_description"); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="categories-tab">
                <? $children = [] ?>
                <? $noChildren = [] ?>
                <?php foreach ($model->categories as $cat) {
                    if ($cat->parent_category != 0) {
                        $children[$cat->id] = $cat->parent_category;
                    }
                } ?>
                <select name="product_category[]" class="multiselect" data-placeholder="Categories" multiple="multiple">
                    <?php foreach ($model->categories as $cat) {
                        foreach (array_unique($children) as $key => $value) {
                            if ($value == $cat->id) {
                                ?>
                                <optgroup label="<?= $cat->name ?>">
                                    <? foreach ($children as $catId => $parentCat) {
                                        if ($parentCat == $cat->id) {
                                            ?>
                                            <option
                                                value="<?= $catId ?>"><? echo \Model\Category::getItem($catId)->name ?></option>
                                        <?
                                        }
                                    } ?>
                                </optgroup>
                            <?
                            }
                        }
                        if ($cat->parent_category == 0) {
                            array_push($noChildren, $cat->id);
                        }
                    } ?>
                    <optgroup label="No Parent">
                        <? foreach (array_values($noChildren) as $value) { ?>
                            <option
                                value="<?php echo $value; ?>"><?php echo \Model\Category::getItem($value)->name; ?></option>
                        <?php } ?>
                    </optgroup>


                </select>
            </div>
            <div role="tabpanel" class="tab-pane" id="images-tab">
                <div class="row">
                    <div class="col-md-24">
                        <div class="box">
                            <div class="dropzone" id="dropzoneForm"
                                 action="<?php echo ADMIN_URL . 'products/upload_images/' . $model->product->id; ?>">

                            </div>
                            <button id="upload-dropzone" class="btn btn-danger">Upload</button>
                            <br/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-24">
                        <div class="box">
                            <table id="image-container" class="table table-sortable-container">
                                <thead>
                                <tr>
                                    <th>Image</th>
                                    <th>File Name</th>
                                    <th>Display Order</th>
                                    <th>Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                <? $prod = new \Model\Product_Image() ?>
                                <?php foreach ($prod->getProductImage($model->product->id, "Product") as $pimg) {

                                    if ($pimg->exists_image()) {
                                        ?>
                                        <tr data-image_id="<?php echo $pimg->id; ?>">
                                            <td><img src="<?php echo $pimg->get_image_url(); ?>" width="100"
                                                     height="100"/></td>
                                            <td><?php echo $pimg->image; ?></td>
                                            <td class="display-order-td"><?php echo $pimg->display_order; ?></td>
                                            <td class="text-center">
                                                <a class="btn-actions delete-product-image"
                                                   href="<?php echo ADMIN_URL; ?>products/delete_prod_image/<?php echo $pimg->id; ?>?token_id=<?php echo get_token(); ?>">
                                                    <i class="icon-cancel-circled"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
    <button type="submit" class="btn btn-save">Save</button>
</form>

<?php echo footer(); ?>
<script type="text/javascript">

    var colors = <?php echo json_encode(explode(',', $model->product->colors)); ?>;
    var sizes = <?php echo json_encode(explode(',', $model->product->sizes)); ?>;
    var categories = <?php echo json_encode($model->product_categories); ?>;
    // var collections = <?php //echo json_encode($model->product_collections); ?>;
    // var materials = <?php //echo json_encode($model->product_materials); ?>;
    //console.log(categories);
    var site_url =<?php echo json_encode(ADMIN_URL.'products/'); ?>;
    $(document).ready(function () {
        //var feature_image = new mult_image($("input[name='featured_image']"),$("#preview-container"));
        var video_thumbnail = new mult_image($("input[name='video_thumbnail']"), $("#preview-thumbnail-container"));

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                var img = $("<img />");
                reader.onload = function (e) {
                    img.attr('src', e.target.result);
                    img.attr('alt', 'Uploaded Image');
                    img.attr("width", '100');
                    img.attr('height', '100');
                };
                $(input).parent().parent().find('.preview-container').html(img);
                $(input).parent().parent().find('input[type="hidden"]').remove();

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("input.image").change(function () {
            readURL(this);
        });


//        $("input[name='name']").on('keyup', function (e) {
//            var val = $(this).val();
//            val = val.replace(/[^\w-]/g, '-');
//            val = val.replace(/[-]+/g, '-');
//            $("input[name='slug']").val(val.toLowerCase());
//        });

        $("select.multiselect").each(function (i, e) {
            $(e).val('');
            var placeholder = $(e).data('placeholder');
            $(e).multiselect({
                nonSelectedText: placeholder,
                includeSelectAllOption: true,
                maxHeight: 415,
                checkboxName: '',
                enableCaseInsensitiveFiltering: true,
                buttonWidth: '100%'
            });
        });
        $("select[name='colors[]']").val(colors);
        $("select[name='sizes[]']").val(sizes);
        $("select[name='product_category[]']").val(categories);
        //$("select[name='product_collection[]']").val(collections);
        //$("select[name='product_material[]']").val(materials);
        $("select.multiselect").multiselect("rebuild");

        function sort_number_display() {
            var counter = 1;
            $('#image-container >tbody>tr').each(function (i, e) {
                $(e).find('td.display-order-td').html(counter);
                counter++;
            });
        }

        $("a.delete-product-image").on("click", function () {
            if (confirm("are you sure?")) {
                location.href = $(this).attr("href");
            } else {
                return false;
            }
        });
        $('#image-container').sortable({
            containerSelector: 'table',
            itemPath: '> tbody',
            itemSelector: 'tr',
            placeholder: '<tr class="placeholder"/>',
            onDrop: function ($item, container, _super, event) {

                if ($(event.target).hasClass('delete-product-image')) {
                    $(event.target).trigger('click');
                }

                var ids = [];
                var tr_containers = $("#image-container > tbody > tr");
                tr_containers.each(function (i, e) {
                    ids.push($(e).data("image_id"));

                });
                $.post(site_url + 'sort_images', {ids: ids}, function (response) {
                    sort_number_display();
                });
                _super($item);
            }
        });

        $("input[name='featured']").on('click', function(){
            if($(this).is(':checked')){
                $("input[name='discount']").parent().show();
            } else {
                $("input[name='discount']").parent().hide();
                $("input[name='discount']").val(0);
            }
        });

    });
</script>
<script type="text/javascript">
    Dropzone.options.dropzoneForm = { // The camelized version of the ID of the form element

        // The configuration we've talked about above
        autoProcessQueue: false,
        uploadMultiple: true,
        parallelUploads: 100,
        maxFiles: 100,
        url: <?php  echo json_encode(ADMIN_URL.'products/upload_images/'.$model->product->id);?>,
        // The setting up of the dropzone
        init: function () {
            var myDropzone = this;

            // First change the button to actually tell Dropzone to process the queue.
            $("#upload-dropzone").on("click", function (e) {
                // Make sure that the form isn't actually being sent.
                e.preventDefault();
                e.stopPropagation();
                myDropzone.processQueue();
            });

            // Listen to the sendingmultiple event. In this case, it's the sendingmultiple event instead
            // of the sending event because uploadMultiple is set to true.
            this.on("sendingmultiple", function () {
                // Gets triggered when the form is actually being sent.
                // Hide the success button or the complete form.
                $("#upload-dropzone").prop("disabled", true);
            });
            this.on("successmultiple", function (files, response) {
                // Gets triggered when the files have successfully been sent.
                // Redirect user or notify of success.
                window.location.reload();
                $("#upload-dropzone").prop("disabled", false);
            });
            this.on("errormultiple", function (files, response) {
                // Gets triggered when there was an error sending the files.
                // Maybe show form again, and notify user of error
            });
        }

    }
</script>