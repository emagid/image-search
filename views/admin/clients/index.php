
	
<div id="page-wrapper" class='clients_page'>
    <h1>Clients</h1>
<!-- 	<a class='settings_link' href="/admin/clients/settings"><img src="<?=FRONT_ASSETS?>img/gears.png"></a>
 -->
    <section class='filters'>
    	<div class="custom-select" style="width:200px;">
		  <select>
		    <option value="0">Filter by...</option>
		    <option value="1">Date</option>
		    <option value="2">Amount of Images</option>
		    <option value="3">Amount of Projects</option>
		    <option value="4">User</option>
		  </select>
		</div>

    	<form class='page_search'>
            <input type="text" name="client_search" placeholder='Search...'>
            <input class='page_submit' type="image" name="submit" src="<?=FRONT_ASSETS?>img/pg_search.png" border="0" />
          </form>
    </section>

	<?php if(count($model->clients) > 0 ) { ?>
	    <section class='table_cards'>
		    <? foreach($model->clients as $client) { 
			    	$projectSql = "Select * from project where client_id = ".$client->id;
			    	$project = \Model\Project::getItem(null,['sql'=>$projectSql]);
			    	// var_dump($projectSql);
			    	$imageSql = "Select * from project_image where client_id = ".$client->id;
			    	$totalCases = \Model\Project::getCount(['where'=>"client_id = $client->id"]);
			    	// var_dump($totalCases);
			    	$totalImages = \Model\Project_Image::getCount(['where'=>"client_id = $client->id"]);
			    	// var_dump($case);
		    ?>
			    	<a href= "<?php echo ADMIN_URL; ?>clients/client/<?= $client->id ?>">
			    		

				    	<div class='table_card client_card table_row card_four'>
				    		<div class='name'>
					    		<h4><?= $client->name ?></h4>
					    		<!-- <h4>Client 1</h4> -->
					    		<p class='card_title'><?php echo date('M d, Y',strtotime($client->insert_time)) ?></p>
				    		</div>

				    		<div class='client_info'>
				    			<div>
						    		<p class='big_text'><?=$totalCases ?></p>
						    		<p class='card_title'>Cases</p>
				    			</div>
				    			<div>
						    		<p class='big_text'><?= $totalImages ?></p>
						    		<p class='card_title'>Images</p>
				    			</div>
				    		</div>
				    	</div>
			    	</a>

		    
			<? } ?>
		</section>
	</div>
<?php } else { ?>
			<section class='table_cards'>
				<div class='table_card'>
					<p>You currently have no clients</p>
				</div>
			</section>
<?php } ?>

<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'clients/index';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>