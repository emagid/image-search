<?php $client = $model->client_details; ?>

<div id="page-wrapper" class='clients_page'>
	<p class='breadcrumb'>Clients /</p>
    <h1><?= $client->name ?></h1>
    <a class='settings_link' href="/admin/clients/settings"><img src="<?=FRONT_ASSETS?>img/gears.png"></a>
    <a class='button loose' href="<?php echo ADMIN_URL; ?>projects/update/<?= urlencode($client->name) ?>">Add Cases&nbsp;
            <i class="icon-plus"></i></a>

        <section class='filters'>
	    	<div class="custom-select" style="width:200px;">
			  <select>
			    <option value="0">Filter by...</option>
			    <option value="1">Date</option>
			    <option value="2">Amount of Images</option>
			    <option value="3">Amount of Projects</option>
			    <option value="4">User</option>
			  </select>
			</div>

	    	<form class='page_search'>
	            <input type="text" name="client_search" placeholder='Search...'>
	            <input class='page_submit' type="image" name="submit" src="<?=FRONT_ASSETS?>img/pg_search.png" border="0" />
	          </form>
	    </section>
	    
    	<?php if(count($model->cases) > 0 ) { 
			foreach($model->cases as $case) {
				// $totImageSql = "Select * from project_image where project_id = $case->id and client_id = $client->id";
				$totalImages = \Model\Project_Image::getCount(['where'=>"project_id = $case->id and client_id= $client->id"]);
				// $compImageSql = "Select * from project_image where project_id = $case->id and client_id = $client->id and status = 1";
				// $totalImages = \Model\Project_Image::getCount(['sql'=>$totimageSql]);
				$completeImage = \Model\Project_Image::getCount(['where'=>"project_id = $case->id and client_id= $client->id and status=1"]);

    	?>
	    
		    
			<a href= "<?php echo ADMIN_URL; ?>projects/project/<?= $case->id ?>">
		    	<div class='table_card client_card table_row card_four'>
		    		<div class='name'>
			    		<h4><?= $case->name ?></h4>
			    		<p class='card_title'><?php echo date('M d, Y',strtotime($case->insert_time)) ?> </p>
		    		</div>
		    		<div class='client_info'>
		    			<div>
				    		<p class='big_text'><?= $totalImages ?></p>
				    		<p class='card_title'>Total</p>
		    			</div>
		    			<div>
				    		<p class='big_text'><?= $completeImage ?></p>
				    		<p class='card_title'>Completed</p>
		    			</div>
		    		</div>
		    	</div>
		    </a>
	    	<? } 
	    } 
	    else { ?>
	    	<section class='table_cards'>
				<div class='table_card'>
					<p>You currently have no cases</p>
				</div>
			</section>
	    <? } ?>
</div>

<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'clients/index';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;



    // SELECT TAG
          var x, i, j, selElmnt, a, b, c;
      /*look for any elements with the class "custom-select":*/
      x = document.getElementsByClassName("custom-select");
      for (i = 0; i < x.length; i++) {
        selElmnt = x[i].getElementsByTagName("select")[0];
        /*for each element, create a new DIV that will act as the selected item:*/
        a = document.createElement("DIV");
        a.setAttribute("class", "select-selected");
        a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
        x[i].appendChild(a);
        /*for each element, create a new DIV that will contain the option list:*/
        b = document.createElement("DIV");
        b.setAttribute("class", "select-items select-hide");
        for (j = 1; j < selElmnt.length; j++) {
          /*for each option in the original select element,
          create a new DIV that will act as an option item:*/
          c = document.createElement("DIV");
          c.innerHTML = selElmnt.options[j].innerHTML;
          c.addEventListener("click", function(e) {
              /*when an item is clicked, update the original select box,
              and the selected item:*/
              var i, s, h;
              s = this.parentNode.parentNode.getElementsByTagName("select")[0];
              h = this.parentNode.previousSibling;
              for (i = 0; i < s.length; i++) {
                if (s.options[i].innerHTML == this.innerHTML) {
                  s.selectedIndex = i;
                  h.innerHTML = this.innerHTML;
                  break;
                }
              }
              h.click();
          });
          b.appendChild(c);
        }
        x[i].appendChild(b);
        a.addEventListener("click", function(e) {
            /*when the select box is clicked, close any other select boxes,
            and open/close the current select box:*/
            e.stopPropagation();
            closeAllSelect(this);
            this.nextSibling.classList.toggle("select-hide");
            this.classList.toggle("select-arrow-active");
          });
      }
      function closeAllSelect(elmnt) {
        /*a function that will close all select boxes in the document,
        except the current select box:*/
        var x, y, i, arrNo = [];
        x = document.getElementsByClassName("select-items");
        y = document.getElementsByClassName("select-selected");
        for (i = 0; i < y.length; i++) {
          if (elmnt == y[i]) {
            arrNo.push(i)
          } else {
            y[i].classList.remove("select-arrow-active");
          }
        }
        for (i = 0; i < x.length; i++) {
          if (arrNo.indexOf(i)) {
            x[i].classList.add("select-hide");
          }
        }
      }
      /*if the user clicks anywhere outside the select box,
      then close all select boxes:*/
      document.addEventListener("click", closeAllSelect);
</script>