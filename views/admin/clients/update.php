<div id="page-wrapper" class='clients_page'>
    <h1>Clients</h1>
	<section class='table_card'>
		<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
		  	<div role="tabpanel">
			    <div class="tab-content">
			      	<div role="tabpanel" class="tab-pane active" id="test">
				        <input type="hidden" name="id" value="<?php echo $model->client->id; ?>" />
				        <input name="token" type="hidden" value="<?php echo get_token();?>" />
				        <div class="row">
				            <div class="col-md-24">
				            	<div class='box'>
				                    <h4>Client Information</h4>

				                    <div class="form-group">
				                        <label class='card_title'>Client Name</label>
				                        <input type="text" name="">
				                    </div>
				                    
				                    <div class="form-group">
				                        <label class='card_title'>Client Company</label>
				                        <?php echo $model->form->editorFor("name"); ?>
				                    </div>
				                    <div class="form-group">
				                        <label class='card_title'>Email</label>
				                        <?php echo $model->form->editorFor("email"); ?>
				                    </div>
				                    <div class="form-group">
				                        <label class='card_title'>Phone</label>
				                        <?php echo $model->form->editorFor("phone"); ?>
				                    </div>

				                    <div class="form-group">
				                        <label class='card_title'>Address</label>
				                        <input type="text" name="">
				                    </div>

				                    <div class="form-group">
				                        <label class='card_title'>City</label>
				                        <input type="text" name="">
				                    </div>

				                    <div class="form-group">
				                        <label class='card_title'>State</label>
				                        <input type="text" name="">
				                    </div>

				                    <div class="form-group">
				                        <label class='card_title'>Notes</label>
				                        <textarea style='max-width: 100%;' type="text" name=""></textarea>
				                    </div>
									  <button type="submit" class="button">Save</button>
				                </div>
				            </div>
				        </div>
				    </div>
			    </div>
		  	</div>
		</form>
	</section>
</div>

<?php echo footer(); ?>
 