<?php
namespace Model;

class Client extends \Emagid\Core\Model {
    static $tablename = 'client';
    public static $fields = [
        'name',
        'email'=>['type'=>'email'],
        'phone',
        'insert_time'
    ];

}