<?php
namespace Model;

class Project extends \Emagid\Core\Model {
    static $tablename = 'project';
    public static $fields = [
        'name',
     	'client_id',
     	'insert_time',
    ];

}