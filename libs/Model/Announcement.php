<?php
namespace Model;

class Announcement extends \Emagid\Core\Model {
    static $tablename = 'announcement';
    public static $fields = [
        'title',
        'description',
        'insert_time',
    ];

}