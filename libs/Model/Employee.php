<?php
namespace Model;

class Employee extends \Emagid\Core\Model {
    static $tablename = 'employee';
    public static $fields = [
        'name'=>['required'=>true],
        'email'=>['required'=>true,'type'=>'email'],
        'phone'=>['required'=>true],
        'business_name'=>['required'=>true],
        'position',
    ];

}