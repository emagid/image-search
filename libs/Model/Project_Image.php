<?php
namespace Model;

class Project_Image extends \Emagid\Core\Model {
    static $tablename = 'project_image';
    public static $fields = [
        'name',
        'image',
        'project_id',
        'client_id',
        'status',
        'insert_time',
    ];

    public function exists_image($size=[]) {
	    $size_str = (count($size)==2) ? implode("_",$size) : "";
	    if($this->image!="" && file_exists(UPLOAD_PATH.'projects'.DS.$size_str.$this->image)) {
	      return true;
	    }
	    return false;
	}

	public function get_image_url($size=[]) {
	    $size_str = (count($size)==2) ? implode("_",$size) : "";
	    return UPLOAD_URL.'projects/'.$size_str.$this->image;
  	}

}