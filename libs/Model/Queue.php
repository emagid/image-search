<?php
namespace Model;

class Queue extends \Emagid\Core\Model {
    static $tablename = 'project_image';
    public static $fields = [
        'image',
        'project_id',
     	'client_id',
     	'insert_time',
    ];

}