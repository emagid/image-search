<?php 
	include("includes/scaffolding.mvc.functions.inc.php");
	$models = [] ;

	if (isset($_POST['_x_em_mvc_scaffold'])) {

		$ds = DIRECTORY_SEPARATOR; 

		$type = $_POST['mvc_scaffold_type']; 

		$path = "views".DIRECTORY_SEPARATOR.$emagid->controller->name; 


		// create the view directory if it doesn't exist .
		if (!file_exists($path)) { 
			mkdir($path) ;
		}

		$path.=DIRECTORY_SEPARATOR.$emagid->controller->view.".php"; 

		if (!file_exists($path))
		{
			$source = __DIR__."{$ds}templates{$ds}scaffold.view.php" ;

			copy($source, $path);
		}


		if ($type=='CAV'){ // needs to create controller 

			#1. check if controller already exists
			$path = "controller{$ds}{$emagid->controller->name}Controller.php" ;

			if (file_exists($path)) {
				$content = file_get_contents ($path) ;
			}else {
				$content = file_get_contents (__DIR__."{$ds}templates{$ds}scaffold.controller.php") ;
				$content = str_replace( '{{name}}', $emagid->controller->name , $content);
			}



			#2 . add the action 
			$length = strlen($content) ;
			$i = -1 ; 
			$index = -1 ;

			while($index < 0 && ($length + $i) > 0 ){
				
				$index = strrpos ($content, '}',$i) ?: -1; 

				$i--; 
			}

			#3. found a match to closing bracket
			if ($index > 0) { 

				#3.a get action template
				$content_action = file_get_contents (__DIR__."{$ds}templates{$ds}scaffold.action.php") ;
				$content_action = str_replace('{{view}}', $emagid->controller->view, $content_action);

				$content = substr_replace($content, $content_action, $index, 0);

				file_put_contents ($path, $content);	
			}
		}



		redirect($_SERVER['REQUEST_URI']);

	}

$folders = [];
$models = [];

if (file_exists('libs/Model'))
	$folders[] = 'libs/Model'; 

foreach ($folders as $folder) {
	$models = array_merge($models, array_filter (array_map(function ($item){ if (! in_array($item, ['.','..','_blank.php'])) { return  str_replace('.php', '' , $item); } },  scandir($folder))));
}

$lmodels = array_map(function($item){ 
	$item = strtolower($item); 

	if (substr($item, -1) == 's'){
		$item = substr($item, 0, -1); 
	}

	return $item;
	
}, $models) ;
// $tables = \Emagid\Core\PdoConn::getTablesList();
?>
<form method="POST" action="#">
<input type="hidden" name="_x_em_mvc_scaffold" value="1" />
<h1>eMagid MVC Scaffolder</h1>
<div>
	eMagid MVC could not locate the controller, and/or the view you requested.
	<br/>
	Please make sure the link is correct, if it does, you may select one of the following actions :
</div>

<div style="margin:20px 0;">
	<label>
		<input type="radio" name="mvc_scaffold_type" value="CAV" checked="checked" /> Generate Controller + Action + View
	</label>

	<?php /* 
	<div style="padding-left:50px; line-height:1.8em; display:none ">
		<div>
			<input type="radio" name="controller_type" value="blank" checked="checked" /> Blank Controller
		</div>
		<div>
			<input type="radio" name="controller_type" value="user_model"  /> Use Model 
			<select name="model_name">
				<option value="">Select Model</option>
				<?php foreach ($models as $item) { ?>
				<option value="<?= $item ?>"><?= $item ?></option>
					
				<?php } ?>
			</select>
		</div>
		<div>
			<input type="radio"  name="controller_type" value="create" /> Create a Model 
			<select name="table_name">
				<option value="">Select Model</option>
				<?php foreach ($tables as $item) { 
					$sitem = $item ; 

					if (substr($sitem, -1) == 's'){
						$sitem = substr($sitem, 0, -1); 
					}
					$disabled = 
						in_array(strtolower($item), $lmodels) || 
						in_array(strtolower($sitem), $lmodels)
						?"disabled=\"disabled\"":"";
				?>
				<option value="<?= $item ?>" <?= $disabled?> ><?= $item ?></option>
					
				<?php } ?>
			

			</select>
		</div>

		<hr/>




	</div>
	*/ ?>
</div>

<div style="margin-bottom:20px;">
	<label>
		<input type="radio" name="mvc_scaffold_type" value="V" />
		Generate Actionless + View
	</label>
</div>
<div style="padding-left:20px;">		
	<input type="submit" value="generate" />
</div>

</form>