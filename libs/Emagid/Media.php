<?php

namespace Emagid;

class Media {

	public $fileType;
	
	public $fileName;
	
	public $fileDir;
	
	public $tmpName;
	
	public $name;
	
	public $alt;
	
	public $title;
	
	public $allowedTypes = [];
	
	public function upload($file, $fileDir){
		if($file && !empty($file) && is_array($file) && $file['error'] == 0){
			$this->fileName = $file['name'];
			$this->tmpName = $file['tmp_name'];
			$this->fileDir = $fileDir;
			
			if(file_exists($this->fileDir.$this->fileName)){
				unlink($this->fileDir.$this->fileName);
			}
			
			$this->fileName = uniqid() . "_" . basename($this->fileName);
			$this->fileName = str_replace(' ', '_', $this->fileName);
			$this->fileName = str_replace('-', '_', $this->fileName);
			
			$this->fileType = explode("/", $file['type']);
			$this->fileType = strtolower(array_pop($this->fileType));
			
			if (!is_dir($this->fileDir)){
				mkdir($this->fileDir, 0777, true);
			}

			if(empty($this->allowedFormats) || in_array($this->fileType, $this->allowedTypes)){
				move_uploaded_file($this->tmpName, $this->fileDir.$this->fileName);
			}
		}
	}
	
	static function delete($file){
		unlink(UPLOAD_PATH.$file);
	}
	
}